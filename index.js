var key = 2;

switch (key) {
  case 1:
    const alpha = require("./alpha");
    break;
  case 2:
    const quadratic_roots = require("./quadratic_roots");
    break;
  case 3:
    const twoD_mat_mul = require("./2d-matrix_multiplication");
    break;
  case 4:
    const threeD_mat_mul = require("./3d-matrix-multiplication");
    break;
  case 5:
    const fly_in_a_box = require("./fly-in-a-box");
    break;

  default:
    break;
}
