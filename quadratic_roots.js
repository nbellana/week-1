function quadratic_roots(a, b, c) {
  var root1, root2;
  if (a === 0) {
    if (b === 0) {
      if (c === 0) {
        console.log("0");
      } else {
        console.log("0");
      }
    } else {
      var root1 = -c / b;
      console.log(root1);
    }
  } else {
    //Calculate discriminant
    var D = b * b - 4 * a * c;
    if (D < 0) {
      //Condition for complex roots
      var realPart = (-b / (2 * a)).toFixed(2);
      var imagPart = (Math.sqrt(-D) / (2 * a)).toFixed(2);

      root1 = realPart + imagPart + "i";
      root2 = realPart - imagPart + "i";

      console.log(root1 + "," + root2);
    } else if (D === 0) {
      //Condition for real and equal roots
      root1 = root2 = -b / (2 * a);
      console.log(root1 + "," + root2);
    } else {
      //Calculation for real roots
      root1 = (-b - Math.sqrt(D)) / (2 * a);
      root2 = (-b + Math.sqrt(D)) / (2 * a);
      console.log(root1 + "," + root2);
    }
  }
}

var roots = quadratic_roots(1, 6, 5);
