/*A new planet was discovered. This is called Planet Alpha. 
Scientists have made a new calendar for this planet and it has only 9 months each and the number of days in each month is below.

January - 30
February - 20
March - 30
April - 25
May - 30
June - 25
July - 30
August - 30
September - 25

Date 02/03/04 means, in the year 04, the 2nd of March. Please note that 26/06/22 is an invalid date, because June has only 25 days. 

Write a function to calculate the difference between two date strings for this planet: "19/02/05" and "22/06/86"
*/

//Function to validate if the given date is in valid format
function validate_date_string(date) {
  let dateformat = /^(0?[1-9]|[1-2][0-9]|30)[\/](0?[1-9])[\/]([0-9]*)$/; //date format

  if (dateformat.test(date)) {
    let day = datePartition(date)[0];
    let month = datePartition(date)[1];
    let year = datePartition(date)[2];

    var monthdays = [30, 20, 30, 25, 30, 25, 30, 30, 25];

    if (day <= monthdays[month - 1]) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

//Partitioning the date
function datePartition(date) {
  var date_partition = date.split("/").map((str) => {
    return Number(str);
  });
  return date_partition;
}

//Function to know the greater date
function greaterDate(firstDate, secondDate) {
  if (validate_date_string(firstDate) && validate_date_string(secondDate)) {
    var date1 = datePartition(firstDate);
    var date2 = datePartition(secondDate);

    if (date1[2] > date2[2]) {
      return 1;
    } else if (date1[2] === date2[2] && date1[1] > date2[1]) {
      return 1;
    } else if (date1[2] === date2[2] && date1[1] === date2[1]) {
      if (date1[0] > date2[0]) {
        return 1;
      } else if (date1[0] === date2[0]) {
        return 0;
      }
    } else {
      return -1;
    }
  } else {
    return "Invalid date";
  }
}

//Function to get the next date
function getNext(date) {
  if (validate_date_string(date) == 1) {
    var date = datePartition(date);
  }

  if (date[0] < 20) {
    date[0] = date[0] + 1;
  } else if (date[0] == 20) {
    if (date[1] == 2) {
      date[0] = 1;
      date[1] = date[1] + 1;
    } else {
      date[0] = date[0] + 1;
    }
  } else if (date[0] >= 21 && date[0] <= 24) {
    date[0] = date[0] + 1;
  } else if (date[0] == 25) {
    if (date[1] == 4 || date[1] == 6) {
      date[1] = date[1] + 1;
      date[0] = 1;
    } else if (date[1] == 9) {
      date[2] = date[2] + 1;
      date[1] = 1;
      date[0] = 1;
    } else {
      date[0] = date[0] + 1;
    }
  } else if (date[0] > 25 && date[0] < 30) {
    date[0] = date[0] + 1;
  } else {
    date[0] = 1;
    date[1] = date[1] + 1;
  }
  if (date[0] < 10) {
    return (
      "0" + String(date[0]) + "/0" + String(date[1]) + "/" + String(date[2])
    );
  }
  return String(date[0]) + "/0" + String(date[1]) + "/" + String(date[2]);
}

//Function to calculate difference between days
function daysCalculator(date1, date2) {
  if (!validate_date_string(date1) || !validate_date_string(date2)) {
    return "Invalid dates";
  }
  if (greaterDate(date1, date2) === 1) {
    [date1, date2] = [date2, date1];
  }
  if (greaterDate(date1, date2) === 0) {
    return 0;
  }
  count = 0;
  while (date1 !== date2) {
    count += 1;
    date1 = getNext(date1);
  }
  return count;
}

console.log(daysCalculator("19/02/05", "22/06/86"));

function getDay(date) {
  if (validate_date_string(date)) {
    var day = datePartition(date)[0];
    var month = datePartition(date)[1];
    var year = datePartition(date)[2];

    var week = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "gooday",
      "badday",
      "luckyday",
    ];

    var months_days = [30, 20, 30, 25, 30, 25, 30, 30, 25];

    var days = day + year * 245;
    for (let i = 0; i < month - 1; i++) {
      days = days + months_days[i];
    }

    var weekday = days % 10;
    var requiredDay = week[weekday];

    return requiredDay;
  } else {
    return "Invalid day";
  }
}

x = getDay("02/01/00");
console.log(x);
